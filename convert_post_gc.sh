#!/bin/sh

# create an optimized bare repo,
# run after 'convert.sh'

git clone --bare file://$PWD/git git_opti
cd git_opti
git gc --aggressive

cd ..
echo "Repo size:" $(du -h git_opti)

